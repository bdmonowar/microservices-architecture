## Application Architecture
The following diagram shows the application architecture after migrating to AWS and containerizing into microservices architecture:
---
![Architecture](microservices architecture deployment solution.png)

## The following AWS services and features now host components of Telia’s new CIM microservice architecture:

- AWS VPN establishes a secure and private tunnel from the on-premises data center to the AWS global network.
- Amazon Route 53 is a Domain Name Server (DNS), which routes global traffic to the application using Elastic Load Balancing.
- Amazon Virtual Private Cloud (Amazon VPC) sets up a logically isolated, virtual network where the application can run securely.
- Application Load Balancer is a product of Elastic Load Balancing, which load balances HTTP/HTTPS applications and uses layer 7-specific features, like port and URL prefix routing for containerized applications.
- Amazon Elastic Container Service (Amazon ECS) is a container orchestration service that supports Docker containers to run and scale containerized applications on AWS.
- AWS Fargate is a compute engine for Amazon ECS that allows running containers without having to manage servers or clusters. Microservices are deployed as Docker containers in the Fargate serverless model.
- Amazon Elastic Container Registry (ECR) is integrated with Amazon ECS as a fully managed Docker container registry that makes it easy to store, manage, and deploy Docker container images. This is used as a private repository to host built-in Docker images.
- Amazon Aurora is a relational database compatible with MySQL and PostgreSQL, used as a database for the CIM platform migration.
- AWS DMS migrates on-premises Oracle databases to cloud-native Aurora databases.
- Amazon CloudWatch is a monitoring and management service used to monitor the entire CIM platform and store application logs for analysis.
- Amazon Elastic Compute Cloud (Amazon EC2) provides compute capacity in the cloud, and was used to host Jenkins and JFrog Artifactory as a container for the CI/CD pipeline.
- AWS Identity and Access Management (IAM) manages access to AWS services and resources securely. In the rest of this post, we describe how each component played a role in modernizing Telia’s CIM application.

## Application Security and Compliance

- An additional layer of network security rule added using network access control list (network ACL) acts as a firewall for controlling traffic in and out at the subnet level.

- The databases and Docker containers are hosted in private subnets and deployed across multiple Availability Zones (AZs) for high availability. To secure all resources, any inbound traffic is blocked and outbound traffic is only allowed using the VPC NAT gateway securely.

- To satisfy audit and compliance needs, the team configured AWS CloudTrail, which provides event history of AWS account activity. The activity includes actions taken through the AWS Management Console, AWS Command Line Interface (CLI), or AWS Software Development Kits (SDK). They also enabled AWS Config to assess, audit, and evaluate the configurations of AWS resources.

## Set Up a CI/CD Pipeline

- To set up the CI/CD pipeline, the team used Jenkins for continuous integration and continuous deployment, with Maven for building. JFrog Artifactory stores Maven-built .jar files and is a universal binary repository manager where you can manage multiple applications, dependencies, and versions in one place.

- Artifactory also enables you to standardize the way you manage package types across all applications developed in your company, no matter the code base or artifact type.

- Amazon EC2 is used to host Jenkins and JFrog Artifactory in Docker containers. Running Jenkins in Docker containers allows the more efficient use of servers running Jenkins worker nodes. It also simplifies the configuration of the worker node servers. Using containers to manage builds allows the underlying servers to be pooled into a cluster. The Docker image is built with the .jar files, stored in ECR, and deployed in Amazon ECS as a Fargate deployment.

- During new deployment/release, the new Docker image is launched first in Fargate and attached to the Application Load Balancer. After it reaches a healthy state, the old Docker container is drained. It’s later removed from the load balancer after all the connections are drained, thus maintaining zero downtime during deployment and releases.